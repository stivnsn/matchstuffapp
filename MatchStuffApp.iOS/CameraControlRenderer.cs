﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using MatchStuffApp.Renderers;

[assembly: ExportRenderer(typeof(CameraControl), typeof(MatchStuffApp.iOS.CameraControlRenderer))]
namespace MatchStuffApp.iOS
{
    public class CameraControlRenderer :
        ViewRenderer<MatchStuffApp.Renderers.CameraControl,
            MatchStuffApp.iOS.CameraControl>
    {
        CameraControl uiCameraPreview;

        protected override void OnElementChanged(ElementChangedEventArgs<MatchStuffApp.Renderers.CameraControl> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                uiCameraPreview = new CameraControl(e.NewElement.Camera);
                SetNativeControl(uiCameraPreview);
            }
            if (e.OldElement != null)
            {
                // Unsubscribe
                uiCameraPreview.Tapped -= OnCameraPreviewTapped;
            }
            if (e.NewElement != null)
            {
                // Subscribe
                uiCameraPreview.Tapped += OnCameraPreviewTapped;
            }
        }

        void OnCameraPreviewTapped(object sender, EventArgs e)
        {
            if (uiCameraPreview.IsPreviewing)
            {
                uiCameraPreview.CaptureSession.StopRunning();
                uiCameraPreview.IsPreviewing = false;
            }
            else
            {
                uiCameraPreview.CaptureSession.StartRunning();
                uiCameraPreview.IsPreviewing = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Control.CaptureSession.Dispose();
                Control.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
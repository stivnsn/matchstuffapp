﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Hardware;
using MatchStuffApp.Renderers;


[assembly: ExportRenderer(typeof(CameraControl), typeof(MatchStuffApp.Droid.CameraControlRenderer))]
namespace MatchStuffApp.Droid
{
    public class CameraControlRenderer :
        ViewRenderer<MatchStuffApp.Renderers.CameraControl,
            MatchStuffApp.Droid.CameraControl>
    {
        CameraControl cameraControl;

        public CameraControlRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(
            ElementChangedEventArgs<MatchStuffApp.Renderers.CameraControl> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                cameraControl = new CameraControl(Context);
                SetNativeControl(cameraControl);
            }

            if (e.OldElement != null)
            {
                // Unsubscribe
                cameraControl.Click -= OnCameraPreviewClicked;
            }
            if (e.NewElement != null)
            {
                try
                {
                    Control.Preview = Camera.Open((int)e.NewElement.Camera);
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("Should add camera permission to assemblyInfo!");
                }

                // Subscribe
                cameraControl.Click += OnCameraPreviewClicked;
            }
        }

        void OnCameraPreviewClicked(object sender, EventArgs e)
        {
            if (cameraControl.IsPreviewing)
            {
                cameraControl.Preview.StopPreview();
                cameraControl.IsPreviewing = false;
            }
            else
            {
                cameraControl.Preview.StartPreview();
                cameraControl.IsPreviewing = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Control.Preview.Release();
            }
            base.Dispose(disposing);
        }
    }
}
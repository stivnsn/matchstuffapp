﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MatchStuffApp.Renderers
{
    public enum CameraOptions
    {
        Rear,
        Front
    }

	public class CameraControl : View
	{
        
        public static readonly BindableProperty CameraProperty = BindableProperty.Create(
            propertyName: "",
            returnType: typeof(CameraOptions),
            declaringType: typeof(CameraControl),
            defaultValue: CameraOptions.Rear);
        public CameraOptions Camera
        {
            get
            {
                return (CameraOptions)GetValue(CameraProperty);
            }
            set
            {
                SetValue(CameraProperty, value); 
}
        }
	}
}